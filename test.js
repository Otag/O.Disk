const Disk = require('./')
const Dene = require('dene')

let Tests = {
  assignAndGet: () => {
    Disk.açar = 'değer'
    debugger
    return Disk.açar == 'değer'
  },
  assignAnObject: () => {
    let Nesne = {
      a: 1
    }
    Disk.açar = Nesne
    return Disk.açar.a == 1
  },
  remove: () => {
    Disk.açar = 5
    delete Disk.açar
    return Disk.açar == null
  },
  expire: () => {
    return new Promise((res, rej)=>{
      let call = {res, rej}
      let Nesne = {
        a: 1
      }
      Disk.açar = Nesne
      Disk.expire('açar', 1)
      let beforeExpire = Disk.açar.a == 1
      setTimeout(() => {call[beforeExpire && Disk.açar == null ? 'res' : 'rej']()}, 2500)
    })
  }
}
Dene(Tests)
