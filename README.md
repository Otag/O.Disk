
# O.Disk

[English Documentation](https://belge.otagjs.org/en/lib/Disk/)

---

O.Disk, yerel yığınak (**localStorage**) üzerine kurulmuş bir soyutlamadır. Her türlü veriyi yazabilmenize olanak tanır. Uzun yazımlardan kurtarır. Yalın ve anlaşılabilir bir arayüz sunar. 

```javascript
O.Disk.açar={
    a: 1
}
// localStorage.setItem(JSON.stringify({a: 1}))
```

**ES6 Bileşeni** olarak da kullanabileceğiniz **O.Disk**'i arka uçta da kullanabilirsiniz.

**Node Bileşeni** olarak kullandığınızda uygulamanızın çalıştığı dizine .odisk/ dizini kurar ve verileri buraya kaydeder.


## Kurulum / Kullanım

```bash
    npm i o.disk -g
```

#### ES6 Bileşeni olarak kullanma

```javascript   
    import Disk from 'o.disk'
```

#### NodeJS Bileşeni olarak kullanma

```javascript   
    const Disk = require('o.disk')
```

> Node Bileşeni olarak kullandığınızda verileri .odisk/ dizinine yazar

#### Geleneksel kullanım biçimi

HTML belgenize ekleyin

```html
<script src="https://cdn.jsdelivr.net/npm/o.disk@2.0.3/index.min.js"></script>
```


## Kullanım

UGA(Uygulama Geliştirme Arabirimi, API) hem ön uçta(tarayıcı) hem de arka uçta(NodeJS) aynı biçimdedir.


### Yığınağa yaz 
```javascript
Disk.açar = 'değer'
```

##### Yığınağa bir Nesne yaz

```javascript
Disk.nesne = {
    açar: 'değer'
}
```

### Yığınaktan oku

```javascript
console.log(Disk.açar) // değer

console.log(Disk.nesne.açar) // değer
```

### Yığınaktan sil

```javascript
delete Disk.açar
console.log(Disk.açar) // null
```

##### Yığınaktan topluca sil

```javascript
Disk.rem(['açar', 'nesne'])
```

##### Yazılan açarı geçici olarak tut

```javascript
Disk.expire('açar', 20) // 20s sonra silinecek
```


## Yeterge

MIT Özgür Yazılım Yetergesi

![](https://nodei.co/npm/o.disk.png)