/*    O.Disk
 *    Copyright © 2018 OtağJS (otagjs.org), MIT License
 *    (https://belge.otagjs.org/en/lib/Disk)
**/
let fs = require('fs')

mkdir('.odisk/expire')

module.exports = global.Disk = new Proxy({
  expire: (k, time) => {
    fs.writeFileSync('.odisk/expire/' + k, now() + Number(time), {flag: 'w+'}, ()=>{})
  },
  rem: k => {
    if(typeof k == 'string') {k = [k]}
    k.forEach(i => {
      rem(i)
    })
  }
}, {
  get: (o, k) => {
    if(k == 'expire' || k == 'rem') {
      return o[k]
    }
    if (typeof k != 'string' || k == 'inspect' ) return
    let e = fs.existsSync('.odisk/expire/' + k) ? fs.readFileSync('.odisk/expire/' + k) : 0
    if(e && Number(e) < now()) {
      delete Disk[k]
      rem(k, 1)
      return null
    }
    k = fs.existsSync('.odisk/' + k) ? fs.readFileSync(('.odisk/' + k), 'utf8') : null
    try{
      return JSON.parse(k)
    }
    catch(Exception) {
      return k
    }
  },
  set: (o, k, v) => {
    fs.writeFileSync('.odisk/' + k, JSON.stringify(v), {flag: 'w+'}, console.warn)
    return true
  },
  deleteProperty: (o, k) => rem(k),
  has: (o, k) => k == 'inspect' ? null : fs.existsSync('.odisk/' + k)
})

// Helper funcs

function rem(path, e) {
  fs.unlinkSync('.odisk/' + (e ? 'expire/' : '') + path)
  return true
}
function now() {
  return Math.floor(+new Date / 1000)
}
function mkdir(path) {
  path.split('/').reduce((prev, i) =>{
    prev += '/' + i
    if(!fs.existsSync(prev)) {
      fs.mkdirSync(prev)
    }
    return prev
  }, '.')
}